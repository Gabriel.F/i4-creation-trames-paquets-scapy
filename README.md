# Pré-requis techniques
* Il est recommandé d'utiliser un environnement Python virtuel (```virtualenv```)
* Si possible exécuter le script en Python 3.5 (version utilisée pour le développement), le script ne fonctionnera pas en 2.X et doit normalement fonctionner en 3.X mais je n'ai pas testé
* Afin d'exécuter les opérations de Scapy sans nécessiter d'être root, veuillez autoriser l'interpréteur Python que vous utilisez avec la commande suivante : ```sudo setcap cap_net_raw=eip /python/path/executable```

# Utilisation de l'application
* L'application étant contenue entièrement dans ```script.py```, il suffit juste d'exécuter ce fichier comme un script Python classique